import Worker from "./Worker.js";
import autoRabbit from "./auto-rabbit.js";

export default class Rabbiteer {
  constructor(ch, workers, options) {
    if (!!workers && !workers.every((c) => c instanceof Worker))
      throw Error("All items must be a 'Worker'!");
    this.ch = ch;
    this.workers = workers ?? [];
    this.options = options;
  }

  async connect() {
    await this._configure(this.options);
  }

  async disconnect() {
    this.ch.close();
    this.conn.close();
  }

  async _configure(options) {
    if (options.autoRabbit) {
      const rabbit = await autoRabbit(options.autoRabbit);
      this.ch = rabbit.ch;
      this.conn = rabbit.conn;
    }
    if (!this.ch) throw Error("No Channel Provided!");
    this._startworkers();
  }

  _startworkers() {
    for (var c of this.workers) {
      c.configure(this.ch);
    }
  }

  async publishMessage(queue, msg, options = {}) {
    if (!this.ch) throw Error("No channel Exists! (Did you run 'connect()'?");
    await this.ch.assertQueue(queue, options.queueOptions ?? {});
    return this.ch.sendToQueue(queue, Buffer.from(msg));
  }

  async publishMessageWithTimeout(queue, msg, timeout = 1000, options = {}) {
    await this.publishMessage(queue, msg, options);
    return new Promise((res) => setTimeout(() => res(), timeout));
  }
}
