const workerDefaults = { autoAck: true, autoJson: true };

export default class Worker {
  constructor(queue, options = workerDefaults) {
    const opt = { ...workerDefaults, ...options };
    this.queue = queue;
    this.autoAck = opt.autoAck;
    this.autoJson = opt.autoJson;
    this.queueOptions = opt.queueOptions ?? {};
    this.ack = () => {};
  }

  async consume(msg, ack) {
    if (this.autoAck) ack();
    else this.ack = ack;

    if (!this.onMessage) return;

    var message = msg.content.toString();
    if (!this.autoJson) return this.onMessage(message);

    try {
      message = JSON.parse(message);
    } catch (e) {
      throw Error(`Couldn’t parse message '${message}' as JSON!\n${e}`);
    }
    return this.onMessage(message);
  }

  async configure(ch) {
    await ch.assertQueue(this.queue, this.queueOptions);
    return ch.consume(this.queue, (msg) =>
      this.consume(msg, () => ch.ack(msg))
    );
  }
}
