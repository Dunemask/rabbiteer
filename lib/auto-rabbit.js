import amqplib from "amqplib";

const reconTimeout = 5000;

const createConnection = (options) => amqplib.connect(options.host, options);

const autoCredentials = (options) => {
  if (options.credentials) return;
  if (!options.pass || !options.user) return;
  options.credentials = amqplib.credentials.plain(options.user, options.pass);
};

const connect = async (options) => {
  const rabbit = {};
  rabbit.conn = await createConnection(options);
  rabbit.ch = await rabbit.conn.createConfirmChannel();

  rabbit.conn.on("error", function autoRabbitError() {
    setTimeout(connect, options.reconnectTimeout ?? reconTimeout);
  });

  return rabbit;
};

export default async function autoRabbit(options) {
  autoCredentials(options);
  return connect(options);
}
