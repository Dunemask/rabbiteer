import { Router, bodyParser } from "express";
export default function workerRouter(rabbiteer) {
  const router = Router();
  router.use(bodyParser());
  router.post("/", function workerRoute(req, res, next) {
    const { queue, msg } = req.body;
    if (!queue) res.status(400).send("Queue was not included in the body!");
    if (!msg) res.status(400).send("Msg was not included in the body!");
    const worker = rabbiteer.workers.find((w) => w.queue === queue);
    if (!worker) res.status(404).send("Queue not found!");
    worker.consume(msg).catch(console.log);
  });
}
