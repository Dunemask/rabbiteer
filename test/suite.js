// Normal imports
import Rabbiteer, { Worker } from "rabbiteer";

// Workers
import LogWorker from "../examples/LogWorker.js"; // Single Worker
import DLXPullWorker from "../examples/DLXPullWorker.js";
import workers from "../examples/workers/index.js"; // Multiple Workers

const logWorker = new LogWorker();
const dlxWorker = new DLXPullWorker();
workers.push(logWorker);
workers.push(dlxWorker);

const rbt = new Rabbiteer(null, workers, {
  autoRabbit: { user: "oasis", pass: "oasis" },
});

await rbt.connect();
await rbt.publishMessageWithTimeout(logWorker.queue, "Hello");
await rbt.publishMessageWithTimeout(logWorker.queue, "World");
await rbt.publishMessageWithTimeout("TwoSecDropQueue", "Laterrr", 2000, {
  queueOptions: {
    messageTtl: 500,
    deadLetterExchange: "DLXExchange",
    deadLetterRoutingKey: "DLXKey",
  },
});
rbt.disconnect();
