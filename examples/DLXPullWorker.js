// Imports
import { Worker } from "rabbiteer";
// Class
export default class DLXPullWorker extends Worker {
  constructor() {
    super("DLXPull", { autoJson: false });
  }

  async configure(ch) {
    await ch.assertExchange("DLXExchange", "direct");
    await ch.assertQueue(this.queue, this.queueOptions);
    await ch.bindQueue(this.queue, "DLXExchange", "DLXKey");
    await ch.consume(this.queue, (msg) => this.consume(msg, () => ch.ack(msg)));
  }

  onMessage(string) {
    console.log(`Died: ${string}`);
  }
}
