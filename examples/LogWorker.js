// Imports
import { Worker } from "rabbiteer";
// Class
export default class LogWorker extends Worker {
  constructor() {
    super("Log", { autoJson: false });
  }

  onMessage(string) {
    console.log(string);
  }
}
