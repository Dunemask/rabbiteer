// Imports
import { Worker } from "rabbiteer";
// Class
export default class LogWorker2 extends Worker {
  constructor() {
    super("Log2", { autoJson: false });
  }

  onMessage(string) {
    console.log("Log2: ", string);
  }
}
