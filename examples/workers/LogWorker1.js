// Imports
import { Worker } from "rabbiteer";
// Class
export default class LogWorker1 extends Worker {
  constructor() {
    super("Log1", { autoJson: false });
  }

  onMessage(string) {
    console.log("Log1: ", string);
  }
}
